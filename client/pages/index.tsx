import { Component } from 'react';
import { connect } from 'react-redux';

import Layout from '../components/Layout';
import PhoneList from '../components/phone/PhoneList';
import { Dispatch } from '../node_modules/redux';
import { phonesActions } from '../store/modules/phones';

interface IndexPageProps {
  dispatch: Dispatch;
}

class IndexPage extends Component<IndexPageProps> {
  render() {
    const { dispatch } = this.props;
    dispatch(phonesActions.phonesRequest());

    return (
      <Layout>
        <PhoneList />
      </Layout>
    );
  }
}

export default connect()(IndexPage);
