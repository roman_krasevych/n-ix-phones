import Document, { Head, Main, NextScript } from 'next/document';

import withStyledComponent from '../HOCs/withStyledComponent';

class MyDocument extends Document {
  render() {
    const { styleTags } = this.props;
    return (
      <html>
        <Head>
          {styleTags}
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
        </Head>
        <body>
          <div id="modal-root" />
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

export default withStyledComponent(MyDocument);
