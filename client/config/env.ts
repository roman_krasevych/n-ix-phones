import { Config, EnvConfig, EnvType } from './types';

const configs: EnvConfig = {
  dev: {
    apiUrl: 'http://localhost:3001/api/',
    nextDevMode: true
  },
  prod: {
    port: 3000,
    isProd: true,
    cdnUrl: '',
    apiUrl: 'http://localhost:3001/api/',
    nextDevMode: false
  }
};

const currentEnv: EnvType = (process.env.NX_ENV as EnvType) || 'dev';
const envConfig: Partial<Config> = configs[currentEnv];

export default envConfig;
