## Quick start

1.  `cd client/`
2.  `yarn install`
3.  `yarn run dev`
4.  open in brouser: `http://localhost:3005/`

## Prod

1.  `yarn run build:prod`
2.  `yarn run server`
3.  open in brouser: `http://localhost:3000/`
