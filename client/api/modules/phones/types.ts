export interface Phone {
  price: number;
  title: string;
  image: string;
  color: string;
  description: string;
}
