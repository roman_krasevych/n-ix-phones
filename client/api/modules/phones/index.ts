import { Request } from '../../types';

import { Phone } from './types';
export * from './types';

export const getPhones = (request: Request) => () =>
  request<Phone[]>({
    method: 'GET',
    url: 'phones'
  });
