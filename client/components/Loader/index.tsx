import { Component } from 'react';
import styled from 'styled-components';

const LoaderWrapper = styled.div`
  width: 93px;
  height: 100px;
  margin: 0 auto;
  display: flex;
  align-items: center;

  > div,
  &:before,
  &:after {
    will-change: transform;
    width: 28px;
    height: 28px;
    content: '';
    background-color: #09bce4;

    border-radius: 100%;
    display: inline-block;
    animation: bouncedelay 1.4s infinite ease-in-out;
    animation-fill-mode: both;
  }

  &:before {
    animation-delay: -0.32s;
    background-color: #0082b9;
  }

  > div {
    animation-delay: -0.16s;
    background-color: #0a86a4;
  }

  @keyframes bouncedelay {
    0%,
    80%,
    100% {
      transform: scale(0);
    }
    40% {
      transform: scale(1);
    }
  }
`;

class Loader extends Component<{}> {
  render() {
    return (
      <LoaderWrapper>
        <div />
      </LoaderWrapper>
    );
  }
}

export default Loader;
