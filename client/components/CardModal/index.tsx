import { Component } from 'react';

import Card from '../Card';
import Modal, { ModalProps } from '../Modal';

import CardModalWrapper from './Wrapper';

export interface CardModalProps extends ModalProps {}

class CardModal extends Component<CardModalProps> {
  render() {
    const { children } = this.props;
    return (
      <Modal {...this.props}>
        <CardModalWrapper>
          <Card>{children}</Card>
        </CardModalWrapper>
      </Modal>
    );
  }
}

export default CardModal;
