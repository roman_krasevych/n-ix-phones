import styled from 'styled-components';

import { flexCenter } from '../../styles/helpers';

const CardModalWrapper = styled.div`
  ${flexCenter};
  pointer-events: all;
`;

export default CardModalWrapper;
