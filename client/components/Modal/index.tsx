import { Component } from 'react';

import Content from './Content';

import Backdrop from './Backdrop';
import ModalPortal from './Portal';

export interface ModalProps {
  onModalClose?: () => void;
}

class Modal extends Component<ModalProps> {
  handleModalClose = () => {
    const { onModalClose } = this.props;
    onModalClose && onModalClose();
  };

  render() {
    const { children } = this.props;
    return (
      <ModalPortal>
        <>
          <Backdrop onClick={this.handleModalClose} />
          <Content> {children}</Content>
        </>
      </ModalPortal>
    );
  }
}

export default Modal;
