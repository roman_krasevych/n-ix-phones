import styled from 'styled-components';

import { flexCenter, stretchBlock } from '../../styles/helpers';

const Content = styled.div`
  pointer-events: none;
  ${flexCenter};
  ${stretchBlock};
`;

export default Content;
