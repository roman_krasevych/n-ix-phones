import styled from 'styled-components';

import { stretchBlock } from '../../styles/helpers';

const Backdrop = styled.div`
  ${stretchBlock};

  position: fixed;
  background-color: rgba(0, 17, 34, 0.7);
  backdrop-filter: blur(5px);
  pointer-events: auto;
`;

export default Backdrop;
