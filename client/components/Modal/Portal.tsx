import { Component } from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';

import isServer from '../../helpers/isServer';

injectGlobal`
#modal-root {
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 999;
  left: 0;
  top: 0;
  pointer-events: none;
}
`;

interface ModalPortalProps {}

class ModalPortal extends Component<ModalPortalProps> {
  el: HTMLDivElement;
  body: HTMLElement;
  modalRoot: HTMLElement;

  constructor(props: ModalPortalProps) {
    super(props);

    if (isServer()) return;

    this.el = document.createElement('div');
    this.modalRoot = document.getElementById('modal-root')!;
    this.body = document.getElementsByTagName('body')[0]!;
  }

  closeModal = () => {
    this.modalRoot.removeChild(this.el);
    this.setOverflow('visible');
  };

  setOverflow(val: string) {
    this.body.style.overflow = val;
  }

  componentDidMount() {
    this.modalRoot.appendChild(this.el);
    this.setOverflow('hidden');
  }

  componentWillUnmount() {
    this.closeModal();
  }

  render() {
    return this.el ? ReactDOM.createPortal(this.props.children, this.el) : null;
  }
}

export default ModalPortal;
