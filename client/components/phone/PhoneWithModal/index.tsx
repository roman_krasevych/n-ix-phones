import { Component } from 'react';

import withModalControls, {
  WithModalControlsProps
} from '../../../HOCs/withModalControls';
import { Phone } from '../../../api/modules/phones';
import CardModal from '../../CardModal';
import PhoneCard from '../PhoneCard';
import PhoneDetails from '../PhoneDetails';

export interface PhoneWithModalProps extends WithModalControlsProps {
  phone: Phone;
}

class PhoneWithModal extends Component<PhoneWithModalProps> {
  render() {
    const { phone, openModal, closeModal, isModalOpened } = this.props;

    return (
      <>
        <div onClick={openModal}>
          <PhoneCard {...phone} />
        </div>

        {isModalOpened && (
          <CardModal onModalClose={closeModal}>
            <PhoneDetails {...phone} />
          </CardModal>
        )}
      </>
    );
  }
}

export default withModalControls(PhoneWithModal);
