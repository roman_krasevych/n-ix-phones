import styled from 'styled-components';

import { media } from '../../../styles/helpers';

const mobileWrapper = media.tablet`
  width: 100%;
  
  > * {
    width: 100%;
  }
`;

export const PhoneListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;

  > * {
    position: relative;
    display: flex;
    width: 300px;
    height: 300px;
    margin: 10px 20px;
  }

  ${mobileWrapper};
`;
const PhoneListWrapper = styled.div``;

export default PhoneListWrapper;
