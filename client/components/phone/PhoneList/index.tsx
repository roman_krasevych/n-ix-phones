import { Component } from 'react';

import withPhones, { WithPhones } from '../../../HOCs/withPhones';
import Container from '../../Container';
import Loader from '../../Loader';
import PhoneWithModal from '../PhoneWithModal';

import PhoneListWrapper, { PhoneListContainer } from './Wrapper';

export interface PhoneListProps extends WithPhones {}

class PhoneList extends Component<PhoneListProps> {
  renderPhoneList() {
    const { phoneList } = this.props;
    if (!phoneList) return null;

    const phoneElements = phoneList.map(phone => (
      <PhoneWithModal key={phone.title} phone={phone} />
    ));

    return <PhoneListContainer>{phoneElements}</PhoneListContainer>;
  }

  render() {
    const { isPhoneListPending } = this.props;
    return (
      <>
        <Container>
          <PhoneListWrapper>
            {isPhoneListPending ? <Loader /> : this.renderPhoneList()}
          </PhoneListWrapper>
        </Container>
      </>
    );
  }
}

export default withPhones(PhoneList);
