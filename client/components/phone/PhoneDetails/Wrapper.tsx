import styled from 'styled-components';

import { flexCenter } from '../../../styles/helpers';

export const PhoneInfoLabel = styled.div`
  margin-top: 10px;
  font-size: 12px;
  color: #aaa;
`;

export const PhoneInfoValue = styled.div`
  font-size: 15px;
  color: #333;
`;

export const PhoneInfoBlock = styled.div`
  padding: 0px 40px;
`;

export const PhonePreview = styled.div`
  ${flexCenter};
  width: 100%;
  height: 100%;

  > img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const PhoneDetailsWrapper = styled.div`
  display: flex;
  height: 50vh;
  padding: 20px 10px;
  align-items: end;
`;

export default PhoneDetailsWrapper;
