import { Component } from 'react';

import { Phone } from '../../../api/modules/phones';

import PhoneDetailsWrapper, {
  PhoneInfoBlock,
  PhoneInfoLabel,
  PhoneInfoValue,
  PhonePreview
} from './Wrapper';

export interface PhoneDetailsProps extends Phone {}

class PhoneDetails extends Component<PhoneDetailsProps> {
  render() {
    const { title, image, color, price, description } = this.props;

    return (
      <PhoneDetailsWrapper>
        <PhonePreview>
          <img src={image} />
        </PhonePreview>

        <PhoneInfoBlock>
          <PhoneInfoLabel>Title:</PhoneInfoLabel>
          <PhoneInfoValue>{title}</PhoneInfoValue>

          <PhoneInfoLabel>Color:</PhoneInfoLabel>
          <PhoneInfoValue>{color}</PhoneInfoValue>

          <PhoneInfoLabel>Price:</PhoneInfoLabel>
          <PhoneInfoValue>{price}$</PhoneInfoValue>

          <PhoneInfoLabel>Description:</PhoneInfoLabel>
          <PhoneInfoValue>{description}</PhoneInfoValue>
        </PhoneInfoBlock>
      </PhoneDetailsWrapper>
    );
  }
}

export default PhoneDetails;
