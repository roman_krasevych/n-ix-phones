import styled from 'styled-components';

import { flexCenter } from '../../../styles/helpers';

export const PhoneTitle = styled.h3`
  text-align: center;
`;

export const PhonePreview = styled.div`
  ${flexCenter};

  width: 100%;
  height: 100%;

  > img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const PhoneCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 10px 20px;
  cursor: pointer;
`;

export default PhoneCardWrapper;
