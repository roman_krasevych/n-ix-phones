import { Component } from 'react';

import { Phone } from '../../../api/modules/phones';
import Card from '../../Card';

import PhoneCardWrapper, { PhonePreview, PhoneTitle } from './Wrapper';

export interface PhoneCardProps extends Phone {}

class PhoneCard extends Component<PhoneCardProps> {
  render() {
    const { title, image } = this.props;
    return (
      <Card>
        <PhoneCardWrapper>
          <PhonePreview>
            <img src={image} />
          </PhonePreview>
          <PhoneTitle>{title}</PhoneTitle>
        </PhoneCardWrapper>
      </Card>
    );
  }
}

export default PhoneCard;
