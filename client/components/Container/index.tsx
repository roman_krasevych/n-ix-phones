import styled from 'styled-components';

import { media } from '../../styles/helpers';

const containerAdaptiveStyles = media.tablet`
    padding: 10px 20px;
`;

const Container = styled.div`
  padding: 20px 30px;
  ${containerAdaptiveStyles};
`;

export default Container;
