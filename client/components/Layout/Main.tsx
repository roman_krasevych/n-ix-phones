import styled from 'styled-components';

const Main = styled.main`
  display: flex;
  flex-direction: column;
  height: 100%;
  min-height: 100vh;
  background-color: #f8f8fa;
`;

export default Main;
