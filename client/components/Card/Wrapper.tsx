import styled from 'styled-components';

const CardWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  min-width: 10px;
  min-height: 10px;
  background: #fff;
  box-shadow: 0px 0px 20px #ccc;
  border-radius: 3px;
`;

export default CardWrapper;
