import { Component } from 'react';

import CardWrapper from './Wrapper';

export interface CardProps {}

class Card extends Component<CardProps> {
  render() {
    const { children } = this.props;
    return <CardWrapper>{children}</CardWrapper>;
  }
}

export default Card;
