import { ComponentType } from 'react';
import { connect } from 'react-redux';

import { ApplicationState } from '../store/createReducer';

import { phonesSelectors } from './../store/modules/phones';

export type WithPhones = ReturnType<typeof mapStateToProps>;

const mapStateToProps = (state: ApplicationState) => ({
  phoneList: phonesSelectors.selectPhones(state),
  isPhoneListPending: phonesSelectors.selectPhonesState(state).isPending
});

const withPhones = <P>(WrappedComponent: ComponentType<P>) =>
  connect(mapStateToProps)(WrappedComponent);

export default withPhones;
