import { combineReducers, Reducer } from 'redux';

import phonesReducer, { PhonesState } from './modules/phones';

export interface ApplicationState {
  phones: PhonesState;
}

const createReducer = (): Reducer<ApplicationState> =>
  combineReducers<ApplicationState>({
    phones: phonesReducer
  });

export default createReducer;
