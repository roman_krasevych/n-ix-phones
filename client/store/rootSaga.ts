import { all } from 'redux-saga/effects';

import { phonesSaga } from './modules/phones';

function* rootSaga() {
  yield all([phonesSaga]);
}

export default rootSaga;
