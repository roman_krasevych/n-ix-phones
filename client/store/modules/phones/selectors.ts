import { ApplicationState } from '../../createReducer';

export const selectPhones = (state: ApplicationState) =>
  selectPhonesState(state).list;
export const selectPhonesState = ({ phones }: ApplicationState) => phones;
