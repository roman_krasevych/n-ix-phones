import { Reducer } from 'redux';

import {
  PhonesActions,
  PHONES_ERROR,
  PHONES_REQUEST,
  PHONES_SUCCESS
} from './actions';
import { PhonesState } from './types';

const initialPhonesState: PhonesState = {
  list: null,
  isError: false,
  isPending: false
};

const phonesReducer: Reducer<PhonesState, PhonesActions> = (
  state = initialPhonesState,
  action
) => {
  switch (action.type) {
    case PHONES_REQUEST: {
      return {
        ...state,
        isError: false,
        isPending: true
      };
    }
    case PHONES_SUCCESS: {
      const { payload: newPhones } = action;

      return {
        ...state,
        list: newPhones,
        isPending: false
      };
    }
    case PHONES_ERROR: {
      const { payload: newError } = action;

      return {
        ...state,
        error: newError,
        isError: true,
        isPending: false
      };
    }
    default:
      return state;
  }
};

export default phonesReducer;
