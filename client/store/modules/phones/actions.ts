import { Phone } from '../../../api/modules/phones';
import { createAction, ActionUnion } from '../../helpers/createAction';

import { ApiError } from '../../../api/types';

export const PHONES_REQUEST = 'N-iX/phones/PHONES_REQUEST';
export const PHONES_SUCCESS = 'N-iX/phones/PHONES_SUCCESS';
export const PHONES_ERROR = 'N-iX/phones/PHONES_ERROR';

export const phonesActions = {
  phonesRequest: () => createAction(PHONES_REQUEST),
  phonesSuccess: (phone: Phone[]) => createAction(PHONES_SUCCESS, phone),
  phonesError: (error: ApiError) => createAction(PHONES_ERROR, error)
};

export type PhonesActions = ActionUnion<typeof phonesActions>;
