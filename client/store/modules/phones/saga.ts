import { call, put, take } from 'redux-saga/effects';

import api from '../../../api';
import { Phone } from '../../../api/modules/phones';

import { phonesActions, PHONES_REQUEST } from './actions';

function* fetchPhones() {
  while (true) {
    yield take(PHONES_REQUEST);
    try {
      const phones: Phone[] = yield call(api().phones.getPhones);
      yield put(phonesActions.phonesSuccess(phones));
    } catch (e) {
      yield put(phonesActions.phonesError(e));
    }
  }
}

const phonesSaga = [call(fetchPhones)];

export default phonesSaga;
