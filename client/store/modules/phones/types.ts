import { Phone } from '../../../api/modules/phones';

import { ResponseState } from './../../types';

export interface PhonesState extends ResponseState {
  list: Phone[] | null;
}
