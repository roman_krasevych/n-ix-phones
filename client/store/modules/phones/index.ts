import phonesReducer from './reducer';
import * as phonesSelectors from './selectors';

export * from './types';
export * from './actions';
export { default as phonesSaga } from './saga';
export { phonesSelectors };

export default phonesReducer;
