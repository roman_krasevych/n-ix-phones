const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes/index");

const app = express();
const port = 3001;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// mount all routes on /api path
app.use("/api", routes);

app.listen(port, () => {
  console.info(`server started on port ${port}`);
});
