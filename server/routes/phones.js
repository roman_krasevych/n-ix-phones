const express = require("express");

const router = express.Router();

const mockPhones = [
  {
    title: "iPhone",
    price: 1000,
    image:
      "https://www.t-mobile.com/images/png/products/phones/Apple-iPhone-X-Silver/250x270_1.png",
    color: "red",
    description: "description of iPhone"
  },
  {
    title: "Samsung",
    price: 155,
    image:
      "http://www.proximus.be/dam/jcr:1f859694-72b0-45af-8637-7158c9f8a7ad/cdn/sites/iportal/images/products/device/p/px-sam-galaxy-s7-g930f-gold-plus-sim/detail/px-sam-galaxy-s7-g930f-gold-plus-sim-XS-1~2018-03-01-15-41-29~cache.png",
    color: "green",
    description: "description of Samsung"
  },
  {
    title: "OnePlus",
    price: 350,
    image:
      "https://staticshop.o2.co.uk/product/images/oneplus-5t-sku-header.png?cb=a1c2633b92f841bdf825d1c718eec321",
    color: "blue",
    description: "description of OnePlus"
  },
  {
    title: "LG",
    price: 250,
    image:
      "http://d2pa5gi5n2e1an.cloudfront.net/global/images/product/mobilephones/LG_G3_mini/LG_G3_mini_L_1.jpg",
    color: "red",
    description: "description of LG"
  },
  {
    title: "Xiaomi",
    price: 110,
    image:
      "https://tradingshenzhen.com/3209-large_default/xiaomi-mimix2s-6gb-64gb.jpg",
    color: "pink",
    description: "description of Xiaomi"
  },
  {
    title: "Asus",
    price: 140,
    image:
      "http://d2pa5gi5n2e1an.cloudfront.net/global/images/product/mobilephones/ASUS_Zenfone_Go_ZB452KG_8GB/ASUS_Zenfone_Go_ZB452KG_8GB_L_1.jpg",
    color: "grey",
    description: "description of Asus"
  },
  {
    title: "Meizu",
    price: 300,
    image:
      "https://drop.ndtv.com/TECH/product_database/images/719201633355PM_635_meizu_mx6.jpeg",
    color: "black",
    description: "description of Meizu"
  },
  {
    title: "Motorola",
    price: 230,
    image: "https://images-na.ssl-images-amazon.com/images/I/51MurKpx6zL.jpg",
    color: "green",
    description: "description of Motorola"
  },
  {
    title: "Lenovo",
    price: 170,
    image: "https://static.toiimg.com/photo/54060996/Lenovo-Phab-2-Plus.jpg",
    color: "blue",
    description: "description of Lenovo"
  },
  {
    title: "Huawei",
    price: 100,
    image:
      "https://i.gadgets360cdn.com/products/large/1521537947_635_huawei_nova_3e.jpg",
    color: "red",
    description: "description of Huawei"
  },
  {
    title: "Nokia",
    price: 50,
    image:
      "https://images-eu.ssl-images-amazon.com/images/I/4118TWTVDHL._SL500_AC_SS350_.jpg",
    color: "blue",
    description: "description of Nokia"
  }
];

router.route("/").get((req, res, next) => {
  return res.json(mockPhones);
});

module.exports = router;
