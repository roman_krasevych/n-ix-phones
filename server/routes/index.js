const express = require("express");
const phones = require("./phones");

const router = express.Router();

router.get("/check", (req, res) => {
  res.send("OK");
});

router.use("/phones", phones);

module.exports = router;
