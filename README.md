# N-iX - Phones

To start with this task, I used my own developed React StartKit that is called - [NextJS-React-Redux-Saga-Axios-StartKit](https://github.com/krasevych/NextJS-React-Redux-Saga-Axios-StartKit)

This project consists of two parts:

1. `client`
2. `server`

You should start from the server first and then run the client, you can read how to do it here: `client/README.md` and `server/README.md`

## Env

I developed and tested this project with node v8.1.3, npm v5.6.0 and yarn v1.5.1, I can't guarantee that it will work with other versions of the technology.
